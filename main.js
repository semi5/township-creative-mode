export function setup(ctx) {
    const CREATIVE_PAGE = 4677;

    const htmlID = (id) => id.replace(/[:_ ]/g, '-').toLowerCase();

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    TownshipUI.yeetResourceOptions.push(10000000);
    TownshipUI.yeetResourceOptions.push(100000000);

    let config = {
        maxTierBuildings: true,
        unlimitedStorage: false,
        allowInvalidBuildings: false,
        freeBuilding: false,
        ignoreBuildingRequirements: false,
        ignoreBiomeRequirements: false,
        ignoreBiomeSizeLimits: false
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    const getTotalMapSize = () => {
        return game.township.biomes.reduce((currentValue, biome) => currentValue+biome.totalInMap, 0);
    };

    const getBuildingCount = () => {
        return game.township.biomes.reduce((currentValue, biome) => currentValue + getBuildingCountBiome(biome), 0);
    };

    const getBuildingCountBiome = (biome) => {
        let buildingSum = 0;
        biome.buildingsBuilt.forEach(build => {
            buildingSum += build;
        });
        return buildingSum;
    }

    const buildBiomeList = () => {
        const container = $('#TS_CREATIVE_BIOME_LIST');
        container.empty();

        game.township.biomes.forEach(biome => {
            const biomeNode = $(`<button class="btn btn-sm btn-outline-success mb-1 w-100 ${ htmlID(biome.id) }">
                <span>${biome.name}</span>
                <small class="qty"></small>
            </button>`);

            biomeNode.on("click", () => { onBiomeSelect(biome); });

            container.append(biomeNode);
        });

        container.append(`<div class="border-top border-crafting mt-4 pt-4">
                <div>Land On Map<div class="float-right state-max-builds">--</div></div>
                <div>Buildings<div class="float-right state-building-count">--</div></div>
                <div>Max Size<div class="float-right">${ game.township.MAX_TOWN_SIZE }</div></div>
            </div>`);

        updateBiomeList();
    };

    const updateBiomeList = () => {
        const maxBuilds = getTotalMapSize();
        let builtBuilds = 0;

        game.township.biomes.forEach(biome => {
            const biomeBuildings = getBuildingCountBiome(biome);
            builtBuilds += biomeBuildings;
            $(`#TS_CREATIVE_BIOME_LIST .${htmlID(biome.id)} .qty`).html(`(${ biome.amountPurchased }) [${ Math.max(biome.totalInMap, biomeBuildings) - biome.availableInMap } / ${ biome.totalInMap }]`);
        });

        $(`#TS_CREATIVE_BIOME_LIST .state-max-builds`).html(`${ maxBuilds }`);
        $(`#TS_CREATIVE_BIOME_LIST .state-building-count`).html(`${ builtBuilds }`);

        updateBiomeButton();
    };

    const updateBiomeButton = () => {
        const id = htmlID(game.township.townData.currentBuildBiome.id);
        $('#TS_CREATIVE_BIOME_LIST .btn-success').toggleClass(['btn-outline-success', 'btn-success']);
        $(`#TS_CREATIVE_BIOME_LIST .${ id }`).toggleClass(['btn-outline-success', 'btn-success']);
    };

    const onBiomeSelect = (biome) => {
        game.township.setBuildBiome(biome);
        updateBiomeButton();
        updateBuildingList();
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    const buildingNodes = new Map();
    const buildingNodesQty = new Map();

    const getBuildingProvideIcons = (building) => {
        let nodes = [];
        if (building.provides.population > 0) {
            nodes.push(`<i class="fa fa-house-user"></i>`);
        }
        if (building.provides.storage > 0) {
            nodes.push(`<i class="fa fa-box-open"></i>`);
        }
        if (building.provides.education > 0) {
            nodes.push(`<i class="fa fa-book-open text-info"></i>`);
        }
        if (building.provides.happiness > 0) {
            nodes.push(`<i class="fa fa-smile" style="color:yellow;"></i>`);
        }
        if (building.provides.worship !== undefined) {
            nodes.push(`<i class="fa fa-church text-danger"></i>`);
        }
        if (building.provides.deadStorage !== undefined) {
            nodes.push(`<i class="fa fa-skull-crossbones text-warning"></i>`);
        }
        return nodes.length > 0 ? `<span class="pill-resource">${ nodes.join(' ') }</span>` : '';
    };

    const getBuildingResourceUsageIcons = (building) => {
        let provides = '';
        let usage = '';
        building.provides.resources.forEach((quantity, resource) => {
            provides += `<img src="${ resource.media }">`;
            if (resource.requires.size > 0) {
                resource.requires.forEach((usedQuantity, usedResource) => {
                    usage += `<img src="${ usedResource.media }">`;
                });
            }
        });

        if(usage.length > 0 && provides.length > 0) {
            usage += ` => `;
        }

        if(usage.length > 0 || provides.length > 0) {
            return `<span class="pill-resource">${ usage }${ provides }</span>`;
        }
        return '';
    };

    const buildBuildingList = () => {
        const container = $('#TS_CREATIVE_BUILDINGS');
        container.empty();

        const buildQty = [1,5,10,25];
        const createBuildQtys = (building, multi, cssClass) => {
            const group = $(`<div class="btn-group-vertical">`);

            buildQty.forEach(val => {
                const buildCount = val * multi;
                const item = $(`<button class="btn btn-sm ${cssClass}">${buildCount < 0 ? buildCount : '+' + buildCount}</button>`);
                item.on('click', () => { onBuildAction(building, buildCount); });
                group.append(item);
            });

            return group;
        }

        game.township.buildingDisplayOrder.forEach(building => {
            const buildingNode = $(`<div class="mr-2 mb-2 p-1 float-left text-center creative-building-item">
                <div class="d-inline-flex align-items-center w-100">
                    <div class="flex-grow-0 build-remove"></div>
                    <div class="flex-grow-1">
                        <div>
                            <img src="${ building.media }" class="resize-32" />
                        </div>
                        <div>
                            <small class="text-warning text-nowrap font-w600">${ building.name }</small>
                        </div>
                        <div>
                            <small class="badge badge-primary qty">0</small>
                        </div>
                        <div class="text-nowrap pt-1">
                            ${ getBuildingProvideIcons(building) }
                            ${ getBuildingResourceUsageIcons(building) }
                        </div>
                    </div>
                    <div class="flex-grow-0 build-add"></div>
            </div>`);

            buildingNode.find('.build-remove').append(createBuildQtys(building, -1, 'btn-outline-danger'));
            buildingNode.find('.build-add').append(createBuildQtys(building, 1, 'btn-outline-success'));

            container.append(buildingNode);

            buildingNodes.set(building.id, buildingNode);
            buildingNodesQty.set(building.id, buildingNode.find('.qty'));
        });

        updateBuildingList();
    }

    const updateBuildingList = () => {
        const biome = game.township.townData.currentBuildBiome;
        game.township.buildingDisplayOrder.forEach(building => {
            const buildingCount = biome.buildingsBuilt.get(building) || 0;

            buildingNodesQty.get(building.id).html(buildingCount);

            const buildingNode = buildingNodes.get(building.id);
            buildingNode.removeClass(['d-none', 'invalid']);
            if (building.biomes.indexOf(biome) == -1) {
                buildingNode.addClass('invalid');
                if (!config.allowInvalidBuildings) {
                    buildingNode.addClass('d-none');
                }
            }

            if(buildingCount > 0) {
                buildingNode.addClass('spell-selected');
            }
            else {
                buildingNode.removeClass('spell-selected');
            }

            if(config.maxTierBuildings && building.upgradesTo != null) {
                buildingNode.addClass('d-none');
            }
        });
    };

    const onBuildAction = (building, qty) => {
        if (qty > 0) {
            addBuilding(building, qty);
        } else if (qty < 0) {
            removeBuilding(building, qty * -1);
        }

        updateBiomeList();
        const biome = game.township.townData.currentBuildBiome;
        const buildingCount = biome.buildingsBuilt.get(building) || 0;
        const buildingNode = buildingNodes.get(building.id);

        buildingNodesQty.get(building.id).html(buildingCount);
        if(buildingCount > 0) {
            buildingNode.addClass('spell-selected');
        }
        else {
            buildingNode.removeClass('spell-selected');
        }
    };

    const addBuilding = (building, qty) => {
        const biome = game.township.townData.currentBuildBiome;
        if (biome === undefined)
            return;

        let qtyToBuild = qty;
        if (!config.ignoreBiomeSizeLimits) {
            qtyToBuild = Math.min(game.township.getAvailableBuildingSpaceInBiome(biome), qty);
        }

        if (qtyToBuild <= 0) {
            console.log(getLangString('TOWNSHIP_MENU', 'NOTICE_4'));
            notifyPlayer(game.township, getLangString('TOWNSHIP_MENU', 'NOTICE_4'), 'danger');
            return;
        }
        if (!config.ignoreBiomeRequirements && !game.township.canBuildInBiome(biome)) {
            console.log(getLangString('TOWNSHIP_MENU', 'NOTICE_13'));
            notifyPlayer(game.township, getLangString('TOWNSHIP_MENU', 'NOTICE_13'), 'danger');
            return;
        }

        biome.availableInMap = Math.max(0, Math.min(biome.totalInMap, biome.availableInMap - qtyToBuild));
        biome.addBuildings(building, qtyToBuild);

        game.township.jobsAreAvailable = true;
        game.township.updateBuildingModifierData(building);
        game.township.computeProvidedStats();
        game.township.updateForBuildingChange();
        townshipUI.updateBuilding(building);
        townshipUI.updateBiomeBreakdown(biome);
        townshipUI.updateBuildingTotalModifierElement(building);
        townshipUI.updateTraderStatus();
        game.township.tasks.updateTownshipBuildingTasks(building, qtyToBuild);
        game.township.tasks.checkForTaskReady(true);
    };

    const removeBuilding = (building, qty) => {
        const biome = game.township.townData.currentBuildBiome;
        if (biome === undefined)
            return;

        const buildingCountInBiome = biome.getBuildingCount(building);
        const qtyToDestroy = Math.min(buildingCountInBiome, qty);

        if (qtyToDestroy > 0) {
            biome.availableInMap = Math.max(0, Math.min(biome.totalInMap, biome.availableInMap + qtyToDestroy));
            biome.removeBuildings(building, qtyToDestroy);

            game.township.computeMaxWorkerCounts();
            game.township.removeOverflowingWorkers(building);
            game.township.updateAllBuildingModifierData();

            game.township.updateUnemployedCount = true;
            game.township.updateForBuildingChange();
            townshipUI.updateBuilding(building);
            townshipUI.updateBiomeBreakdown(biome);
            townshipUI.updateBuildingTotalModifierElement(building);
            townshipUI.updateTraderStatus();
        }
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    const changeWorship = (worship) => {
        game.township.townData.worship = worship;
        game.township.updateAllBuildingModifierData();
        game.township.updateForBuildingChange();
        game.township.computeProvidedStats();
        game.township.biomes.forEach((biome) => {
            townshipUI.updateBiomeBreakdown(biome);
        });
        townshipUI.updateWorship();
        townshipUI.updateForBuildQtyChange();

        const statueBuilding = game.township.buildings.getObjectByID('melvorF:Statues');
        const statueNode = buildingNodes.get('melvorF:Statues');
        statueNode.find('img').attr('src', statueBuilding.media);
        statueNode.find('.text-warning').html(statueBuilding.name);
    };

    const createDropdown = (label, items, callback) => {
        const dropdown = $(`<div class="dropdown mb-2">
            <button class="btn btn-sm btn-outline-info dropdown-toggle w-100" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${label}</button>
            <div class="dropdown-menu dropdown-menu-bottom overflow-y-auto font-size-sm" style="max-height: 60vh; z-index: 9999;">

            </div>
        </div>`);

        const list = dropdown.find('.dropdown-menu');
        items.forEach(item => {
            const htmlItem = $(`<button class="dropdown-item" type="button">${item[1]}</button>`);
            htmlItem.on('click', () => { callback(item[0]); });
            list.append(htmlItem);
        });

        return dropdown;
    };

    const createToggle = (label, defaultValue, callback) => {
        const toggleID = htmlID(label);
        const toggle = $(`<div class="custom-control custom-switch custom-control mb-2 font-size-sm">
            <input class="custom-control-input" type="checkbox" name="semi-auto-bury-enable-check" id="ts-creative-${toggleID}">
            <label class="font-weight-normal ml-2 custom-control-label" for="ts-creative-${toggleID}">${label}</label>
        </div>`);

        const input = toggle.find('.custom-control-input');
        input.on('change', () => { callback(input)});
        input.prop('checked', defaultValue);

        return toggle;
    };

    const buildActionList = () => {
        const container = $('#TS_CREATIVE_ACTIONS');

        const actionBuyAllLand = $(`<button class="btn btn-sm mb-2 w-100 btn-outline-success"><span>Buy All Land</span></button>`);
        actionBuyAllLand.on('click', () => {
            game.township.townData.sectionsPurchased = game.township.MAX_TOWN_SIZE;
            game.township.biomes.forEach(biome => {
                biome.amountPurchased = biome.totalInMap;
                townshipUI.updateBiomeBreakdown(biome);
            });
            updateBiomeList();
            updateBuildingList();
        });
        container.append(actionBuyAllLand);

        const actionMaxCitizens = $(`<button class="btn btn-sm mb-2 w-100 btn-outline-success"><span>Max Citizens</span></button>`);
        actionMaxCitizens.on('click', () => {
            const ticksFor100 = Math.ceil(99 * (261 / (game.township.TICK_LENGTH / 10)));
            const citizensToAdd = Math.max(0, game.township.populationLimit - game.township.currentPopulation);
            for (let i = 0; i < citizensToAdd; i++) {
                const citizen = {
                    job: game.township.unemployedJob,
                    ticksAlive: Math.round(Math.random() * ticksFor100),
                    source: CitizenSource.Birth,
                };
                game.township.citizens.push(citizen);

                if (game.township.getCitizenAge(citizen) >= 55)
                    game.township.popOver55++;
            }
            game.township.jobsAreAvailable = true;
            game.township.updateUnemployedCount = true;
            game.township.renderQueue.extraBuildingRequirements = true;
            updateForChange();
        });
        container.append(actionMaxCitizens);

        const actionClearDead = $(`<button class="btn btn-sm mb-2 w-100 btn-outline-success"><span>Clear Dead Storage</span></button>`);
        actionClearDead.on('click', () => {
            game.township.townData.dead = 0;
        });
        container.append(actionClearDead);

        const worshipList = game.township.worships.allObjects.map(worship => [worship, worship.name]);
        container.append(createDropdown('Change Worship', worshipList, function(worship) {
            changeWorship(worship);
        }));

        container.append(`<hr>`);

        const actionResetBuildTicks = $(`<button class="btn btn-sm mb-2 w-100 btn-outline-success"><span>Reset Build Ticks</span></button>`);
        actionResetBuildTicks.on('click', () => {
            game.township.availableGameTicksToSpend = 0;
            townshipUI.updateTicksAvailable();
        });
        container.append(actionResetBuildTicks);

        const tickLengths = [1, 12, 144, 288, 500, 1000, 5000, 25000].map(ticks => [ticks, `${ticks}`]);
        container.append(createDropdown('Add Build Ticks', tickLengths, function(ticks) {
            game.township.availableGameTicksToSpend += ticks;
            townshipUI.updateTicksAvailable();
        }));

        container.append(`<hr>`);

        const actionImportLayout = $(`<button class="btn btn-sm mb-2 w-100 btn-outline-success"><span>Import Layout</span></button>`);
        actionImportLayout.on('click', () => {
            Swal.fire({
                title: 'Import Layout',
                input: 'text',
                inputValue: '',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                customClass: {
                    input: 'text-white'
                },
                showCancelButton: true,
                confirmButtonText: 'Import'
            }).then((result) => {
                if (result.isConfirmed) {
                    const oldLayout = exportLayout();
                    try {
                        importLayout(JSON.parse(result.value));
                        notifyPlayer(game.township, `Layout Imported.`, 'danger');
                    } catch (e) {
                        importLayout(oldLayout);
                        notifyPlayer(game.township, `Unable to import layout.`, 'danger');
                    }
                }
            });
        });
        container.append(actionImportLayout);

        const actionExportLayout = $(`<button class="btn btn-sm mb-2 w-100 btn-outline-success"><span>Export Layout</span></button>`);
        actionExportLayout.on('click', () => {
            let preset = JSON.stringify(exportLayout());
            navigator.clipboard.writeText(preset).then(() => {
                notifyPlayer(game.township, `Copied to clipboard!`, 'success');
            }, () => {
                notifyPlayer(game.township, `Unable to copy to clipboard.`, 'danger');
            });
        });
        container.append(actionExportLayout);

        container.append(`<hr>`);

        const toggleMaxTierBuildings = createToggle('Max Tier Buildings Only', config.maxTierBuildings, function(e) {
            config.maxTierBuildings = $(e).prop('checked');
            updateBuildingList();
        });
        container.append(toggleMaxTierBuildings);

        const toggleAllowInvalidBuildings = createToggle('Allow Invalid Buildings', config.allowInvalidBuildings, function(e) {
            config.allowInvalidBuildings = $(e).prop('checked');
            updateBuildingList();
        });
        container.append(toggleAllowInvalidBuildings);

        const toggleUnlimitedStorage = createToggle('Unlimited Storage', config.unlimitedStorage, function(e) {
            config.unlimitedStorage = $(e).prop('checked');
            updateForChange();
        });
        container.append(toggleUnlimitedStorage);

        const toggleFreeBuilding = createToggle('No Building Costs', config.freeBuilding, function(e) {
            config.freeBuilding = $(e).prop('checked');
            updateForChange();
        });
        container.append(toggleFreeBuilding);

        const toggleIgnoreBuildingRequirements = createToggle('Ignore Building Requirements', config.ignoreBuildingRequirements, function(e) {
            config.ignoreBuildingRequirements = $(e).prop('checked');
            updateForChange();
        });
        container.append(toggleIgnoreBuildingRequirements);

        const toggleIgnoreBiomeSizeLimits = createToggle('Ignore Biome Size Limits', config.ignoreBiomeSizeLimits, function(e) {
            config.ignoreBiomeSizeLimits = $(e).prop('checked');
            updateBuildingList();
        });
        container.append(toggleIgnoreBiomeSizeLimits);

        const toggleIgnoreBiomeRequirements = createToggle('Ignore Biome Requirements', config.ignoreBiomeRequirements, function(e) {
            config.ignoreBiomeRequirements = $(e).prop('checked');
            updateBuildingList();
        });
        container.append(toggleIgnoreBiomeRequirements);

        container.append(`<hr>`);

        const mapList = game.township.maps.filter(map => map.biomeCounts.size > 0).map(map => [map, map.name]);
        container.append(createDropdown('Change Map', mapList, function(map) {
            game.township.destroyAllBuildings();

            game.township.biomes.forEach((biome) =>{
                const biomeCount = map.getBiomeCount(biome);
                biome.totalInMap = biomeCount;
                biome.availableInMap = biomeCount;
                biome.amountPurchased = 0;
            });

            const grasslands = game.township.biomes.getObjectByID('melvorF:Grasslands');
            game.township.townData.currentBuildBiome = grasslands;
            game.township.generateStartingSection(grasslands);
            game.township.generateEmptyTown(grasslands);
            game.township.updateGeneratedTownBreakdown();

            updateForChange();
            updateBiomeList();
            updateBuildingList();
        }));
        container.append(`<div class="text-warning w-100 font-size-sm text-center">Changing maps will clear all buildings</div>`);

        container.append(`<hr>`);

        const tierList = Object.entries(game.township.populationForTier).map(tier => [tier[1].level, `${tier[1].level} (Tier ${tier[0]})`]);
        tierList.push([120, '120 (Max Level)']);
        container.append(createDropdown('Change Township Level', tierList, function(level) {
            game.township.setXP(exp.level_to_xp(level) + 1);
        }));

        const actionDeleteAll = $(`<button class="btn btn-sm mb-2 w-100 btn-outline-danger"><span>Delete all Buildings</span></button>`);
        actionDeleteAll.on('click', () => {
            game.township.destroyAllBuildings();
            updateBiomeList();
            updateBuildingList();
        });
        container.append(actionDeleteAll);
    };

    const update = () => {
        updateBiomeList();
        updateBiomeButton();
        updateBuildingList();
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    const importLayout = (layout) => {
        console.log(layout);

        if (layout.townData) {
            if (layout.townData.worship) {
                const layoutWorship = game.township.worships.getObjectByID(layout.townData.worship);
                if (layoutWorship) {
                    changeWorship(layoutWorship);
                }
            }
        }

        if (layout.biomes) {
            game.township.destroyAllBuildings();
            game.township.biomes.forEach(biome => {
                if (layout.biomes[biome.id]) {
                    const lb = layout.biomes[biome.id];

                    biome.amountPurchased = lb.amountPurchased || 0;
                    biome.availableInMap = lb.availableInMap || 0;
                    biome.totalInMap = lb.totalInMap || 0;

                    if (lb.buildings != null) {
                        lb.buildings.forEach(entry => {
                            const lbBuilding = game.township.buildings.getObjectByID(entry.id);
                            const lbQty = entry.qty || 0;
                            if (lbBuilding && lbQty > 0) {
                                biome.buildingsBuilt.set(lbBuilding, lbQty);
                            }
                        });
                    }
                }
            });
        }

        updateForChange();
        updateBiomeList();
        updateBuildingList();
    };

    const exportLayout = () => {
        let output = {
            townData: {
                worship: game.township.townData.worship.id,
                dead: game.township.townData.dead,
                biomesUnlocked: game.township.townData.biomesUnlocked,
                sectionsPurchased: game.township.townData.sectionsPurchased,
            },
            biomes: {},
            resources: {}
        };

        game.township.biomes.forEach(biome => {
            let biomeData = {
                amountPurchased: biome.amountPurchased,
                availableInMap: biome.availableInMap,
                totalInMap: biome.totalInMap,
                buildings: []
            }

            biome.buildingsBuilt.forEach((value, building) => {
                biomeData.buildings.push({
                    id: building.id,
                    qty: value
                })
            });

            output.biomes[biome.id] = biomeData;
        });

        game.township.resources.forEach(resource => {
            if (resource.type == 0) return;
            output.resources[resource.id] = Math.floor(resource.amount);
        });

        return output;
    };

    const updateForChange = () => {
        game.township.updateAllBuildingModifierData();
        game.township.updateForBuildingChange();
        game.township.computeProvidedStats();
        game.township.biomes.forEach((biome) =>{
            townshipUI.updateBiomeBreakdown(biome);
        });
        townshipUI.updateTownStats();
        townshipUI.updateTraderStatus();
    };

    const injectUI = () => {
        const navItem = `
            <li class="nav-main-item">
                <a class="nav-main-link" data-toggle="class-toggle" data-target="#horizontal-navigation-township" data-class="d-none" id="TS_CUSTOM_BTN_CREATIVE">
                    <img class="skill-icon-sm m-0 mr-2" src="assets/media/main/settings_header.svg">
                    <span class="nav-main-link-name font-w600">Creative</span>
                </a>
            </li>`;
        $('#township-category-menu').find('.nav-main').append(navItem);
        townshipUI.defaultElements.btn.psyCreative = document.getElementById('TS_CUSTOM_BTN_CREATIVE');
        townshipUI.defaultElements.btn.psyCreative.addEventListener('click', () =>{
            townshipUI.showPage(CREATIVE_PAGE);
        });

        const creativePage = `<div id="TS_CUSTOM_DIV_CREATIVE" class="d-none">
            <div class="col-12 font-w400" id="TS_CUSTOM_MAIN_SAVE_WARNING">
                <div class="alert alert-danger p-1 font-size-md w-100 text-center">
                    <div class="mb-1"><i class="fa fa-fw fa-exclamation-circle mr-2"></i>Creative Mode is intended to be used on a seperate save then you would normally play on. There is always a chance this tool will BREAK YOUR SAVE, so use with caution.</div>
                    <div class="mb-1"><button class="btn btn-sm btn-danger" id="TS_CUSTOM_MAIN_SAVE_WARNING_DISMISS"><span>Dismiss</span></button></div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-xl-2 col-lg-4 col-md-12 pr-2 font-size-sm">
                    <div class="border-bottom border-crafting text-warning w-100 pb-1 mb-2 font-size-base">Biomes</div>
                    <div id="TS_CREATIVE_BIOME_LIST"></div>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-12 pl-2 pr-2">
                    <div class="border-bottom border-crafting text-warning w-100 pb-1 mb-2 font-size-base">Buildings</div>
                    <div id="TS_CREATIVE_BUILDINGS"></div>
                </div>
                <div class="col-xl-2 col-lg-12 col-md-12">
                    <div class="border-bottom border-crafting text-warning w-100 pb-1 mb-2 font-size-base">Actions</div>
                    <div id="TS_CREATIVE_ACTIONS"></div>
                </div>
            </div>
        </div>`;
        $('#DIV_CONTAINER').children().first().append(creativePage);
        townshipUI.defaultElements.div.psyCreative = document.getElementById('TS_CUSTOM_DIV_CREATIVE');

        $('#TS_CUSTOM_MAIN_SAVE_WARNING_DISMISS').on('click', () => {
            $('#TS_CUSTOM_MAIN_SAVE_WARNING').addClass('d-none');
            // TODO: Save Dismiss
        });

        buildBiomeList();
        buildBuildingList();
        buildActionList();
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ctx.onInterfaceReady(() => {
        injectUI();

        ctx.patch(TownshipUI, 'getPageButton').after(function(result, page) {
            if (page == CREATIVE_PAGE) {
                return this.defaultElements.btn.psyCreative;
            }
            return result;
        });

        ctx.patch(TownshipUI, 'showPage').after(function(result, pageID) {
            this.defaultElements.div.psyCreative.classList.add('d-none');
            if (pageID == CREATIVE_PAGE) {
                update();
                this.defaultElements.div.psyCreative.classList.remove('d-none');
            }
        });

        /**
         * Prevent building outside the biome limit from granting more available space
         * on building destroy then exist on the map.
         */
        ctx.patch(Township, 'removeBuildingFromBiome').after(function(result, biome, building, count = 1) {
            if (biome.availableInMap > biome.totalInMap) {
                biome.availableInMap = biome.totalInMap;
            }
        });

        /**
         * Override Max Storage to basically unlimited.
         */
        ctx.patch(Township, 'getMaxStorage').after(function(result) {
            if (config.unlimitedStorage) {
                return Math.max(result, 999999999999);
            }
            return result;
        });

        /**
         * Override Building Cost to be Free.
         */
        ctx.patch(Township, 'modifyBuildingResourceCost').replace(function(oldFunction, quantity) {
            if (config.freeBuilding) {
                return 0;
            }
            return oldFunction(quantity);
        });

        /**
         * Override Building Requirements such as Skill Level and Current Population.
         */
        ctx.patch(Township, 'canBuildTierOfBuilding').replace(function(oldFunction, building, notify = false) {
            if (config.ignoreBuildingRequirements) {
                return true;
            }
            return oldFunction(building, notify);
        });
    });
}
